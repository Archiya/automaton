import numpy as np
from numpy import genfromtxt

class Neural_Network():
    def __init__(self):
        np.random.seed(1)
        self.W = 2*np.random.random((3,1))-1
    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))
    def sigmoid_derivative(self, x):
        return x * (1-x)
    def train(self, X, y, i):
        for iteration in range(i):
            output = self.think(X)
            error = y - output
            adjustment = np.dot(X.T, error * self.sigmoid_derivative(output))
            self.W += adjustment
    def think(self, X):
        X = X.astype(float)
        y = self.sigmoid(np.dot(X, self.W))
        return y

if __name__ =="__main__":
    NN = Neural_Network()
    X = genfromtxt('X_train.csv', delimiter=',')
    y = genfromtxt('Y_train.csv', delimiter=',')
    X = np.reshape(X, (-1, 3))
    y = np.reshape(y, (-1, 1))

    NN.train(X, y, 10000)
    A = 1
    B = 1
    C = 1
    print(NN.think(np.array([A, B, C])))